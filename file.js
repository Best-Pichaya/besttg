var fs = require('fs');

const read = function (file, callback) {
  fs.readFile(file, 'utf8', (err, msg) => {
    if (err) if(callback) return callback(err, msg)
    if(callback) callback(null, msg)
  });
}

const write = function (file, str, callback) {
  fs.writeFile(file, str, 'utf8', (err) => {
    if (err) if(callback) return callback(true, err)
    if(callback) callback(null, true)
  });
}

module.exports = {
  read: read,
  write: write
}
var fm = require('./file')
var config = './config.json'
var git_repo = process.argv[2]

function append() {
    if (!git_repo) return
    fm.read(config, (err, msg) => {
        if (err) console.log(err, msg)
        var json = msg ? JSON.parse(msg) : []
	var index = json.findIndex(x=>x.url===git_repo)
	if(index === -1){
	    json.push({
                timestamp: null,
                url: git_repo
            })
            fm.write(config, JSON.stringify(json))
            console.log('added...' + git_repo + '...done...')
        }
        else{
            console.log('url exist...')
        }
    })
}

append()

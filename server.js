/* 
run: node server.js [git_repository]
node server.js https://trieolsm:9638521a@github.com/trieolsm/test2.git

[
    {
        "timestamp": null,
        "url": "https://trieolsm:9638521a@github.com/trieolsm/test2.git"
    }
]

*/

"use strict";

var schedule = require('node-schedule')
var process = require('process')
var exec = require("child_process").exec
var fm = require('./file')

var template = 'https://github.com/trieolsm/running.git'
var workingDir = './worker/'
var config = './config.json'
var duration = 20 // minute
var file = 'updatetime'
var myjob
var lock = false

function trigger(git_repo, timeNow, callback) {
    var dir = workingDir + getName(git_repo)
    console.log('job start on .... ' + dir + '_' + timeNow)
    process.chdir(dir)
    var cmd = 'git remote set-url origin ' + git_repo
    exec(cmd, () => {
        fm.write(file, timeNow)
        cmd = 'git pull && git add . && git commit -m "add_' + timeNow + '" && git push'
        exec(cmd, () => {
            process.chdir('../../')
            if (callback) callback()
        })
    })
}
function getName(git_repo) {
    var spliter = '.com/'
    if(git_repo.indexOf('.org') > -1) spliter = '.org/';
    return git_repo.split(spliter).pop().split('.')[0].replace('/', '_')
}
function run(git_repo, timeNow, callback) {
    var name = getName(git_repo)
    var dir = workingDir + name
    var cmd = 'rm -rf ' + dir + ' && git clone ' + git_repo + ' ' + dir
    console.log('git cloning to...' + dir)
    exec(cmd, () => {
        trigger(git_repo, timeNow, callback)
    })
}

function checkTime(timeNow, compare) {
    if (!compare) return true
    return (timeNow - compare) >= (1000 * 60 * duration)
}

function loop(array, timeNow, index, callback) {
    if (index >= array.length) return callback(array)
    var item = array[index]
    if (checkTime(timeNow, item.timestamp)) {
        item.timestamp = timeNow
        run(item.url, timeNow, () => {
            loop(array, timeNow, ++index, callback)
        })
    }
    else{
        loop(array, timeNow, ++index, callback)
    }
}

function start() {
    if (!lock) {
        lock = true
        fm.read(config, (err, msg) => {
            if (err) console.log(err, msg)
            const json = JSON.parse(msg)
            const timeNow = (new Date()).getTime()
            loop(json, timeNow, 0, (data) => {
                fm.write(config, JSON.stringify(data), () => {
                    lock = false
                    console.log('job done.')
                })
            })
        })
    }
}
// start process, checking every 1 minutes
console.log('running....')
myjob = schedule.scheduleJob('*/1 * * * *', start);
//j.cancel();

var fm = require('./file')
var config = './config.json'
var git_repo = process.argv[2]

function remove() {
    if (!git_repo) return
    fm.read(config, (err, msg) => {
        if (err) console.log(err, msg)
        var json = JSON.parse(msg)
        var index = json.findIndex(x=>x.url===git_repo)
        if(index > -1) json.splice(index, 1)
        fm.write(config, JSON.stringify(json))
        console.log('remove...' + git_repo + '...done...')
    })
}

remove()
